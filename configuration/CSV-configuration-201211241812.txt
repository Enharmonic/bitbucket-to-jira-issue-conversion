{
  "field.Labels" : "labels",
  "field.Updated" : "updated",
  "project.lead" : "jsaggau@enharmonichq.com",
  "value.Issue Type.enhancement" : "4",
  "field.Priority" : "priority",
  "field.Summary" : "summary",
  "value.Assigned.dillan" : "dlaughlin",
  "date.import.format" : "yyyy-MM-dd HH:mm:ss",
  "field.Reporter" : "reporter",
  "value.Priority.blocker" : "1",
  "project.name" : "enh-topsOrtho",
  "value.Issue Type.task" : "3",
  "value.Labels.Question Index" : "Question Index",
  "value.Status.resolved" : "5",
  "value.Priority.trivial" : "5",
  "value.Issue Type.bug" : "1",
  "field.Issue Type" : "issuetype",
  "field.Status" : "status",
  "user.email.suffix" : "@enharmonichq.com",
  "value.Labels.Questions" : "Questions",
  "value.Labels.Check-in & Check-out" : "Check-in & Check-out",
  "field.Assigned" : "assignee",
  "value.Priority.minor" : "4",
  "field.Description" : "description",
  "date.fields" : "Updated",
  "field.Created" : "created",
  "value.Reporter.dillan" : "dlaughlin",
  "value.Status.duplicate" : "6",
  "value.Priority.major" : "3",
  "value.Status.new" : "1",
  "project.key" : "ENHTOPS",
  "value.Labels.UI" : "UI",
  "mapfromcsv" : "false",
  "value.Labels.Form Selection" : "Form Selection",
  "value.Labels.Form Review" : "Form Review"
}